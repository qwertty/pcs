# CrackMapExec
CrackMapExec (a.k.a CME) is a post-exploitation tool that helps automate assessing the security of _large_ Active Directory networks. Built with stealth in mind, CME follows the concept of "Living off the Land": abusing built-in Active Directory features/protocols to achieve it's functionality and allowing it to evade most endpoint protection/IDS/IPS solutions.
[GitHub Repo](https://github.com/byt3bl33d3r/CrackMapExec)

It could also be used for `password spraying`.
## Usage
```text
$ cme --help
usage: cme [-h] [-v] [-t THREADS] [--timeout TIMEOUT] [--jitter INTERVAL]
           [--darrell] [--verbose]
           {http,smb,mssql} ...

      ______ .______           ___        ______  __  ___ .___  ___.      ___      .______    _______ ___   ___  _______   ______
     /      ||   _  \         /   \      /      ||  |/  / |   \/   |     /   \     |   _  \  |   ____|\  \ /  / |   ____| /      |
    |  ,----'|  |_)  |       /  ^  \    |  ,----'|  '  /  |  \  /  |    /  ^  \    |  |_)  | |  |__    \  V  /  |  |__   |  ,----'
    |  |     |      /       /  /_\  \   |  |     |    <   |  |\/|  |   /  /_\  \   |   ___/  |   __|    >   <   |   __|  |  |
    |  `----.|  |\  \----. /  _____  \  |  `----.|  .  \  |  |  |  |  /  _____  \  |  |      |  |____  /  .  \  |  |____ |  `----.
     \______|| _| `._____|/__/     \__\  \______||__|\__\ |__|  |__| /__/     \__\ | _|      |_______|/__/ \__\ |_______| \______|

                                         A swiss army knife for pentesting networks
                                    Forged by @byt3bl33d3r using the powah of dank memes

                                                      Version: 4.0.0dev
                                                     Codename: 'Sercurty'

optional arguments:
  -h, --help         show this help message and exit
  -v, --version      show program's version number and exit
  -t THREADS         set how many concurrent threads to use (default: 100)
  --timeout TIMEOUT  max timeout in seconds of each thread (default: None)
  --jitter INTERVAL  sets a random delay between each connection (default: None)
  --darrell          give Darrell a hand
  --verbose          enable verbose output

protocols:
  available protocols

  {http,smb,mssql}
    http             own stuff using HTTP(S)
    smb              own stuff using SMB and/or Active Directory
    mssql            own stuff using MSSQL and/or Active Directory

Serrrrrrcuuurrrty?

```