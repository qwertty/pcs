# Useful cmdlet's

## Comparison and Logical Operators
Use PowerShell’s logical operators to compare pieces of data and make decisions based on them.

**Comparison operators:**
```
-eq, -ne, -ge, -gt, -in, -notin, -lt, -le, -like, -notlike, -match, -notmatch, -contains, -notcontains, -is, -isnot
```

**Logical operators:**
```
-and, -or, -xor, -not
```

**Examples**
```powershell
# Example 1
PS > (dir).Count -ge 4
True

# Example 2
PS > "Hello World" -match "H.*World"
True

# Example 3
PS > $data = "Hello World"
PS > ($data -like "*llo W*") -and ($data.Length -gt 10)
True
PS > ($data -like "*llo W*") -and ($data.Length -gt 20)
False
```

## New-Object
To create an instance of an object using its default constructor, use the `New-Object` cmdlet with the class name as its only parameter:

```powershell
$generator = New-Object <CLASSNAME>
```

To create an object and use it at the same time (without saving it for later), wrap the call to `New-Object` in parentheses:

```powershell
# Example: (New-Object <CLASSNAME>).<METHOD>()
(New-Object Net.WebClient).DownloadString("https://example.com")
```

## Get-Member
You have an instance of an object and want to know what methods and properties it supports?
The most common way to explore the methods and properties supported by an object is through the `Get-Member` cmdlet.

```powershell
$object | Get-Member
# OR
Get-Member -InputObject $object
```

### Get static members
To get the static members of an object you’ve stored in the `$object` variable, supply the `-Static` flag to the `Get-Member` cmdlet:

```powershell
$object | Get-Member -Static
# OR
Get-Member -Static -InputObject $object
```

### Get members of a specific type
To get members of the specified member type, supply that member type to the `-MemberType` parameter:

```powershell
$object | Get-Member -MemberType <TYPE>
# OR 
Get-Member -MemberType <TYPE> -InputObject $object
```


## Set and Get clipboard content
To interact with the Windows clipboard use the `Get-Clipboard` or `Set-Clipboard` cmdlet's.

**Set clipboard content:**
```powershell
PS > "Hello World" | Set-Clipboard
PS > dir | Set-Clipboard
# OR
PS > Set-Clipboard "Hello World"
```

**Get clipboard content:**
```powershell
PS > Get-Clipboard
Hello World
```
